#!/bin/bash

if [ "$2" == "" ];
	then exit 1
fi

if (( ${2} < 1 || ${2} > 999 ))
	then exit 1
fi

file_name="$1"
shift

if ! [ -f "${file_name}" ]
	then exit 1
fi

Array=( "$@" )

for i in ${Array[@]}
do
	if ! [[ ${i} =~ ^[0-9]{1,3}$ && ${i} != 0 ]]
		then exit 1
	fi
done

for i in ${Array[@]}; do
	cat ${file_name} | while read line; do
		if [[ ${line} =~ ^"${i}"\;.*$ ]]; then
			length=0
			cnt=0
			tmp=

			for ((j = 0; j < ${#line}; j++)); do
				c=${line:${j}:1}

				if [[ ${c} =~ \; ]]; then
					cnt=$(( cnt + 1 ))
					length=$((length + tmp))
					tmp=
				else if [ $((cnt % 3)) == 2 ]; then
						tmp=${tmp}${c}
					fi
				fi
			done
			echo "${i};${length}"
		fi
	done
done

