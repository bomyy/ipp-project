/** @file
 * Interfejs klasy przechowującej miasto.
 *
 * @author Konrad Czapliński <kc406122@mimuw.edu.pl>
 */
#ifndef __CITY_H__
#define __CITY_H__

typedef struct City City;

///@cond INTERNAL
extern const int VISITED;
extern const int NOT_VISITED;
extern const int NOT_DISTINCT;
///@endcond

#include "name.h"
#include "set.h"

/**
* struktura odpowiedzialna za implementację klasy @p Set
*/
typedef struct City {
	Set* neighbour; ///< wskaźnik na zbiór sąsiadów
	String* name; ///< wskażnik na nazwę miasta
	struct City* predecessor; ///< wskaźnik na poprzednika na ścieżce
	unsigned long long weight; ///< minimalna odległość od startowego wierzchołka
	/**
	ze wszystkich minimalnych odległości,
	jest to ścieżka gdzie najstarsza droga była jak najmłodsza
	*/
	int year;
	/**
	ze wszystkich minimalnych odległości,
	jest to dróga ścieżka gdzie najstarsza droga była jak najmłodsza
	*/
	int secondYear;
	int status; ///< status odwiedzenia miasta
} City;

/**
tworzy nowe miasto o nazwie @p name
@param [in] name - nazwa nowego miasta
@return zwraca wskaźnik na nowe miasto lub NULL jeśli nie
udoło się zaalokować pamięci
*/
City* newCity(const char* name);

/**
@brief zwraca nazwę danego miasta

@param [in] city - wskaźnik na miasto o które pytamy
@return funkcja zwraca wskaźnik nazwę danego miasta
*/
String* getName(City* city);

/**
@brief usuwa miasto

funkcja usuwa miasto oraz jego nazwę.

@param [in] city - wskaźnik na miasto które usuwamy
*/
void delCity(City* city);

/**
@brief porównuje czy nazwa miasta a jest ściśle mniejsza od miasta b

@param [in] a - wskaźnik na pierwsze miasto
@param [in] b - wskaźnik na drugie miasto
@return jeśli nazwa miasta a jest ściśle mniejsza leksykograficznie
od miasta b zwraca true, w przeciwnym wypadku zwraca false
*/
bool compLowerBound(City* a, City* b);

/**
@brief porównuje czy nazwa miasta a jest mniejsza równa od miasta b

@param [in] a - wskaźnik na pierwsze miasto
@param [in] b - wskaźnik na drugie miasto
@return jeśli nazwa miasta a jest mniejsza  lub równa leksykograficznie
od miasta b zwraca true, w przeciwnym wypadku zwraca false
*/
bool compUpperBound(City* a, City* b);

/**
@brief porównuje czy nazwa miasta a równa nazwie b

@param [in] a - wskaźnik na pierwsze miasto
@param [in] b - wskaźnik na drugie miasto
@return jeśli nazwa miasta a jest równa nazwie miasta b zwraca true,
w przeciwnym wypadku zwraca false
*/
bool isSame(City* a, City* b);

/**
@brief zwraca wskaźnik na zbiór sąsiadów miasta @p city

@param [in] city - wskaźnik na miasto o które pytamy
@return zwraca wskaźnik na set sąsiadów miasta @p city lub
NULL jeśli @p city nie ma żadnych sąsiadów
*/
Set* getNeighbour(City* city);

/**
@brief ustala zbiór sąsiadów miasta

ustala zbiór sąsiadów miasta @p city na @p set.

@param [in] city - wskaźnik na miasto któremu ustawiamy set sąsiadów
@param [in] set - wskaźnik na set sąsiadów
*/
void setNeighbour(City* city, Set* set);

/**
@brief ustala odległość na @p dis oraz rok na @p year

ustala dla miasta @p city odległość na @p dis oraz rok na @p year

@param [in] city - wskaźnik na miasto któremu ustawiamy odległość i rok
@param [in] dis - odległość na jaką ustawiamy
@param [in] year - rok na jaką ustawiamy
*/
void setDis(City* city, long long dis, int year);

/**
@brief zwraca wartość secondYear

zwraca wartość secondYear miasta @p city
@param [in] city - wskaźnik na miasto o które pytamy
@return zwraca wartość secondYear dla miasta @p city
*/
int getSecondYear(City* city);

/**
@brief ustala wartość secondYear na @p year

ustala dla miasta @p city secondYear na @p year

@param [in] city - wskaźnik na miasto któremu ustawiamy secondYear
@param [in] year - rok na jaki ustawiamy
*/
void setSecondYear(City* city, int year);

/**
@brief zwraca wartość year

zwraca wartość year miasta @p city
@param [in] city - wskaźnik na miasto o które pytamy
@return zwraca wartość year dla miasta @p city
*/
int getYear(City* city);

/**
@brief zwraca wartość weight

zwraca wartość weight miasta @p city
@param [in] city - wskaźnik na miasto o które pytamy
@return zwraca wartość weight dla miasta @p city
*/
long long getDis(City* city);

/**
@brief ustala wartości weight, year, secondYear oraz predecessor na wartości początkowe

ustala dla miasta @p city weight na LLONG_MAX, year na INT_MAX,
secondYear na INT_MIN oraz predecessor na NULL;

@param [in] city - wskaźnik na miasto któremu ustawiamy wartości na początkowe
*/
void prepareSet(City* city);

/**
@brief zwraca wartość status

zwraca wartość status miasta @p city
@param [in] city - wskaźnik na miasto o które pytamy
@return zwraca wartość status dla miasta @p city
*/
int getStatus(City* city);

/**
@brief ustala wartość status na @p status

ustala dla miasta @p city status na @p status

@param [in] city - wskaźnik na miasto któremu ustawiamy status
@param [in] status - status na jaki ustawiamy
*/
void setStatus(City* city, int status);

/**
@brief zwraca wartość Predecessor

zwraca wartość Predecessor miasta @p city
@param [in] city - wskaźnik na miasto o które pytamy
@return zwraca wskaźnik na miasto które jest poprzednikiem,
lub NULL jeśli nie zostało ono ustalone
*/
City* getPredecessor(City* city);

/**
@brief ustala wartość predecessor na @p pre

ustala dla miasta @p city predecessor na @p pre

@param [in] city - wskaźnik na miasto któremu ustawiamy predecessor
@param [in] pre - wskaźnik na miasto które ustawiamy jako poprzednik @p city
*/
void setPredecessor(City* city, City* pre);
#endif