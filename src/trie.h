/** @file
* Interfejs klasy przechowującej drzewo Trie
*
* @author Konrad Czapliński <kc406122@mimuw.edu.pl>
*/
#ifndef __TRIE_H__
#define __TRIE_H__

#include "list.h"

/**
@brief struktura odpowiedzialna za implementację drzewa trie.
*/
typedef struct Trie Trie;

/// tworzy nowe, puste drzewo @p Trie i je zwraca
Trie* newNode();

/**
@brief podpina do danego wierzchołka @p trie podaną @p listę

@param[in] nr - numer wierzchołka do którego dodajemy drogę
@param[in] list - lista którą dodajemy
@param[in] trie - wierzchołek drzewa trie do którego dodajemy listę.
@return Zwraca wartość @p true jeśli podpięcie się powiodło, i @p false
jeśli zabrakło pamięci lub dany wierzchołek posiada już listę.
*/
bool addTrie(int nr, List* list, Trie* trie);

/**
@brief zwraca wierzchołek @p Trie o podanym numerze

@param[in] routeId - numer wierzchołka o który pytamy
@param[in] trie - wierzchołek drzewa trie na którym zadajemy zapytanie.
@return Zwraca wartość @p Trie jeśli wierzchołek o podanym numerze istnieje
i @p NULL w przeciwnym wypadku
*/
Trie* findTrie(int routeId, Trie* trie);

/**
@brief usuwa całe drzewo Trie
Usuwa całe poddrzewo na które wskazuje trie, wywołując się rekurencyjnie

@param[in] trie - wierzchołek drzewa trie które chcemy usunąć.
*/
void delTrie(Trie* trie);

/**
@brief przypina podaną listę @p list do podanego wierzchołka @p trie

@param[in] list - wskaźnik na liską do przypięcia
@param[in] trie - wierzchołek do którego przypinamy listę
*/
void setList(Trie* trie, List* list);

/**
@brief zwraca wskaźnik na @p listę danego wierzchołka

@param[in] trie - wierzchołek drzewa trie na którym zadajemy zapytanie.
@return Zwraca wartość @p List jeśli wierzchołek @p trie jest niepusty
i @p NULL w przeciwnym wypadku
*/
List* getList(Trie* trie);

/**
@brief podłącza drzewo @p trie1 do drzewa @p trie2

wywołuje się rekurencyjnie, w danym wierzchołku iteruje się po liście aż do
znalezienia elementu równego pierwszemu elementowi z listy w @p trie2
a następnie dołącza listę @p trie2 do listy @p trie1
@param[in] trie1 - wskaźnik na wierzchołek do którego podłączamy @p trie2
@param[in] trie2 - wierzchołek który podłączamy
*/
void mergeTrie(Trie* trie1, Trie* trie2);
#endif