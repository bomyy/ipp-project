/** @file
 * Interfejs klasy odpowiedzialnej za wczytywanie danych
 *
 * @author Konrad Czapliński <kc406122@mimuw.edu.pl>
 */
#ifndef __READ_H__
#define __READ_H__

#include "map.h"

/**
@brief sprawdza czy całe wejście zostało wczytane

@return zwraca @p true jeśli całe wejście zostało już wczytane,
lub @p false w przeciwnym wypadku
*/
bool isEndOfInput();

/**
@brief wczytuje kolejną linię wejścia

@return zwraca liniię wejścia jako const char*.
Jeśli całe wejście zostało już wczytane zawraca pusty napis.
*/
const char* readLine();

/**
@brief wyszukuje kolejny znak ';' lub znak końca napisu

@param [in] str - napis który będziemy przeszukiwać
@param [in] id - indeks znaku od którego mamy szukać znaku

@return zwraca indeks pierwszego znaku ';' o indeksie większym
niż podane id, lub indeks znaku końca napisu jeśli w napisie nie
ma znaku ';' o indeksie większym niż id.
*/
size_t nextStop(const char* str, size_t id);

/**
@brief zamienia podnapis na inta

funkcja zamienia podnapis @p str zaczynając od @p start na inta.
zamienia wszystkie znaki zaczynając od @p start włącznie, do napotkania
znaku ';' lub znaku końca napisu.

@param [in] str - napis który chcemy zamienić na inta
@param [in] start - indeks od którego chcemy zamieniać podnapis
na inta

@return zwraca inta. Funkcja nie przewiduje co się stanie jeśli dany
podnapis nie jest intem.
*/
int getInt(const char* str, size_t start);

/**
@brief sprawdza czy dany napis jest poprawny, a następnie wywołuje addRoad

funkcja sprawdza czy napis jest poprawnym opisem funkji addRoad,
a następnie wywołuje funkcję addRoad z podanymi argumentami.

@param [in] map - wskaźnik na mapę na której wywołujemy operację
@param [in] str - napis, który jest opisem operacji addRoad

@return funkcja zwraca @p false jeśli podany napis jest nie poprawny
lub nie powiedzie się operacja addRoad. W przypadku powodzenia funkcji zwraca
@p true.
*/
bool addRoadLine(Map* map, const char* str);

/**
@brief sprawdza czy dany napis jest poprawny, a następnie wywołuje addRoute

funkcja sprawdza czy napis jest poprawnym opisem funkji addRoute,
a następnie wywołuje funkcję addRoute z podanymi argumentami.

@param [in] map - wskaźnik na mapę na której wywołujemy operację
@param [in] str - napis, który jest opisem operacji addRoute

@return funkcja zwraca @p false jeśli podany napis jest nie poprawny
lub nie powiedzie się operacja addRoute. W przypadku powodzenia funkcji zwraca
@p true.
*/
bool addRouteLine(Map* map, const char* str);

/**
@brief sprawdza czy dany napis jest poprawny, a następnie wywołuje getRouteDescription

funkcja sprawdza czy napis jest poprawnym opisem funkji getRouteDescription,
a następnie wywołuje funkcję getRouteDescription z podanymi argumentami.

@param [in] map - wskaźnik na mapę na której wywołujemy operację
@param [in] str - napis, który jest opisem operacji getRouteDescription

@return funkcja zwraca @p false jeśli podany napis jest nie poprawny
lub nie powiedzie się operacja getRouteDescription. W przypadku powodzenia funkcji zwraca
@p true.
*/
bool getRouteLine(Map* map, const char* str);

/**
@brief sprawdza czy dany napis jest poprawny, a następnie wywołuje repairRoad

funkcja sprawdza czy napis jest poprawnym opisem funkji repairRoad,
a następnie wywołuje funkcję repairRoad z podanymi argumentami.

@param [in] map - wskaźnik na mapę na której wywołujemy operację
@param [in] str - napis, który jest opisem operacji repairRoad

@return funkcja zwraca @p false jeśli podany napis jest nie poprawny
lub nie powiedzie się operacja repairRoad. W przypadku powodzenia funkcji zwraca
@p true.
*/
bool repairRoadLine(Map* map, const char* str);

#endif