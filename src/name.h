/** @file
 * Interfejs klasy przechowującej nazwę miasta
 *
 * @author Konrad Czapliński <kc406122@mimuw.edu.pl>
 */
#ifndef __NAME_H__
#define __NAME_H__


/**
* @brief struktura odpowiedzialna za implementację Stringa.
*/
typedef struct String String;

/**
@brief tworzy nowego stringa i go zwraca
@param [in] name - nazwa jaką ma przechowywać nowy string
@return tworzy nowego string i go zwraca, jeśli nie udało się zaalokować
pamięci to zwraca NULL.
*/
String* newName(const char* name);

/**
@brief usuwa stringa
usuwa wskazanego stringa, wraz z tablicą charów
@param [in] name - wskaźnik na stringa do usunięcia
*/
void delString(String* name);

/**
@brief porównuje stringi
@param [in] name1 - wskaźnik na pierwszego stringa
@param [in] name2 - wskaźnik na drugiego stringa
@return poruwnuje czy string name1 jest ściśle mniejszy od
name2, zwraca @p true jeśli tak jest i @p false w
przeciwnym wypadku
*/
bool lowerBound(String* name1, String* name2);

/**
@brief porównuje stringi
@param [in] name1 - wskaźnik na pierwszego stringa
@param [in] name2 - wskaźnik na drugiego stringa
@return poruwnuje czy string name1 jest mniejszy równy od
name2, zwraca @p true jeśli tak jest i @p false w
przeciwnym wypadku
*/
bool upperBound(String* name1, String* name2);

/**
@brief zwraca tablicę charów przechowywaną przez stringa
@param [in] name - wskaźnik na stringa
@return zwraca wskaźnik na tablicę charów którą przechowywać string
*/
const char* getString(String* name);

/**
@brief zwraca rozmiar stringa
@param [in] name - wskaźnik na stringa
@return zwraca z ilu elementów składa się podany string
*/
size_t getSize(String* name);
#endif