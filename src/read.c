/** @file
implementacja klasy odpowiedzialej za wczytywanie danych.
*/
/**
define srawia że przy kompilacji nie dostaje się warningów na temat strdup.
Bez niej pojawiają się warningi o braku deklaracji strdup, mimo dodania string.h
 */
#define _DEFAULT_SOURCE 1
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <limits.h>
#include <ctype.h>
#include "map.h"
#include "read.h"

///@cond INTERNAL
static const char addRoadArr[] = "addRoad;";
static const char repairRoadArr[] = "repairRoad;";
static const char getRouteArr[] = "getRouteDescription;";
static bool isEofReached;

static bool isLastSemicolon(const char* str)
{
	size_t id = 0;
	for(; str[id] != '\0'; id++);
	if(id == 0) return true;
	if(str[id - 1] == ';') return true;
	return false;
}

static bool isInt(const char* str, size_t start)
{
	int64_t tmp = 0;
	int64_t sign = 1;

	if(str[start] == '-') {
		sign = -1;
		start ++;
	}

	size_t cnt = 0;
	for(size_t i = start; ; i++) {
		if(str[i] == ';') return (cnt > 0);
		if(str[i] == '\0') return (cnt > 0);
		if(!isdigit(str[i])) return false;
		tmp *= 10;
		tmp += str[i] - '0';
		if(tmp * sign > INT_MAX) return false;
		if(tmp * sign < INT_MIN) return false;
		cnt ++;
	}

	return (cnt > 0);
}

static bool isRouteId(const char* str, size_t start)
{
	if(!isInt(str, start)) return false;
	int tmp = getInt(str, start);
	if(tmp < 1) return false;
	if(tmp > 999) return false;
	return true;
}

static bool isLength(const char* str, size_t start)
{
	if(!isInt(str, start)) return false;
	int tmp = getInt(str, start);
	if(tmp < 1) return false;
	return true;
}

static bool isYear(const char* str, size_t start)
{
	if(!isInt(str, start)) return false;
	int tmp = getInt(str, start);
	if(tmp == 0) return false;
	return true;
}

static bool isName(const char* str, size_t start)
{
	if(str[start] == '\0') return false;

	int cnt = 0;
	for(size_t i = start; ; i++) {
		if(str[i] == ';') return (cnt > 0);
		if(str[i] == '\0') return (cnt > 0);
		if(str[i] >= 0 && str[i] <= 31) return false;
		cnt ++;
	}

	return (cnt > 0);
}

const char* readLine()
{
	size_t lineSize = 4;
	size_t id = 0;
	char* line = calloc(lineSize, sizeof(char));
	if(line == NULL) return NULL;

	while(1) {
		int c = getchar();
		if(c == '\n') break;

		if(c == EOF) {
			isEofReached = true;
			break;
		}

		if(lineSize == id) {
			lineSize *= 2;
			char* tmp = realloc(line, lineSize * sizeof(char));
			if(tmp == NULL) {
				free(line);
				return NULL;
			}
			line = tmp;
		}

		if(c == '\0') c = 1;

		line[id] = c;
		id ++;
	}

	const char* ret = (const char*)strndup(line, id);
	free(line);
	return ret;
}

///@endcond
bool isEndOfInput()
{
	return isEofReached;
}

size_t nextStop(const char* str, size_t id)
{
	if(str[id] == '\0' || str[id] == ';') return id;

	while(1) {
		if(str[id] == '\0') return id;
		if(str[id] == ';') return id + 1;
		id ++;
	}

	// kod nigdy do tego momentu nie powinnien dojść,
	// aby nie było warning'ów dodane jest exit(1)
	exit(1);
}

int getInt(const char* str, size_t start)
{
	int64_t tmp = 0;
	int64_t sign = 1;

	if(str[start] == '-') {
		sign = -1;
		start ++;
	}

	for(size_t i = start; ; i++) {
		if(str[i] == '\0') break;
		if(str[i] == ';') break;
		tmp *= 10;
		tmp += str[i] - '0';
	}

	return tmp * sign;
}

bool addRoadLine(Map* map, const char* str)
{
	if(isLastSemicolon(str)) return false;
	size_t id = 0;
	for(; addRoadArr[id] != '\0'; id++) {
		if(str[id] != addRoadArr[id]) return false;
	}

	int city1 = id;
	int city2 = nextStop(str, city1);
	int length = nextStop(str, city2);
	int year = nextStop(str, length);
	int eol = nextStop(str, year);
	if(str[year] == '\0') return false;
	if(str[eol] != '\0') return false;

	if(!isName(str, city1)) return false;
	if(!isName(str, city2)) return false;
	if(!isLength(str, length)) return false;
	if(!isYear(str, year)) return false;

	char* c1 = (char*)strndup(&str[city1], city2 - city1 - 1);
	char* c2 = (char*)strndup(&str[city2], length - city2 - 1);
	bool msg = false;

	if(c1 != NULL && c2 != NULL) {
		msg = addRoad(map, c1, c2, getInt(str, length), getInt(str, year));
	}

	free(c1);
	free(c2);
	return msg;
}

bool addRouteLine(Map* map, const char* str)
{
	if(isLastSemicolon(str)) return false;
	size_t routeId = 0;
	size_t city1 = nextStop(str, routeId);
	size_t akt = nextStop(str, city1);

	if(!isRouteId(str, 0)) return false;
	if(!isName(str, city1)) return false;

	size_t cnt = 0;
	while(true) {
		int length = akt;
		int year = nextStop(str, length);
		int name = nextStop(str, year);
		akt = nextStop(str, name);

		if(!isLength(str, length)) return false;
		if(!isYear(str, year)) return false;
		if(!isName(str, name)) return false;
		cnt ++;

		if(str[akt] == '\0') break;
	}

	if(cnt == 0) return false;
	return addRoute(map, str);
}

bool getRouteLine(Map* map, const char* str)
{
	if(isLastSemicolon(str)) return false;
	size_t id = 0;
	for(; getRouteArr[id] != '\0'; id++) {
		if(str[id] != getRouteArr[id]) return false;
	}

	int routeId = id;
	int eol = nextStop(str, id);
	if(str[routeId] == '\0') return false;
	if(str[eol] != '\0') return false;

	if(!isInt(str, routeId)) return false;
	if(getInt(str, routeId) < 0) return false;

	const char* route = getRouteDescription(map, getInt(str, routeId));

	if(route == NULL) {
		return false;
	} else {
		printf("%s\n", route);
		free((char*)route);
		return true;
	}
}

bool repairRoadLine(Map* map, const char* str)
{
	if(isLastSemicolon(str)) return false;
	int id = 0;
	for(; repairRoadArr[id] != '\0'; id++) {
		if(str[id] != repairRoadArr[id]) return false;
	}

	int city1 = id;
	int city2 = nextStop(str, city1);
	int year = nextStop(str, city2);
	int eol = nextStop(str, year);
	if(str[year] == '\0') return false;
	if(str[eol] != '\0') return false;

	if(!isName(str, city1)) return false;
	if(!isName(str, city2)) return false;
	if(!isYear(str, year)) return false;

	const char* c1 = (const char*)strndup(&str[city1], city2 - city1 - 1);
	const char* c2 = (const char*)strndup(&str[city2], year - city2 - 1);
	bool msg = false;

	if(c1 != NULL && c2 != NULL) {
		msg = repairRoad(map, c1, c2, getInt(str, year));
	}

	free((char*)c1);
	free((char*)c2);
	return msg;
}
