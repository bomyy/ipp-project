/** @file
Implementacja klasy przechowującej drzewo Trie
*/
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "trie.h"

/**
@brief struktura odpowiedzialna za implementację drzewa Trie.

Trie przechowuje dwa wskaźniki na synów, nazwane 'left' oraz 'right'
, oraz element typu list.
*/
typedef struct Trie {
	Trie* left; ///< wskaźnik na lewego syna
	Trie* right; ///< wskaźnik na prawego syna
	List* list; ///< wskaźnik na listę
} Trie;

Trie* newNode()
{
	return calloc(1, sizeof(Trie));
}

bool addTrie(int nr, List* list, Trie* trie)
{
	while(nr != 0) {
		if(nr % 2 == 0) {
			if(trie->left == NULL) trie->left = newNode();
			if(trie->left == NULL) return false;
			trie = trie->left;
		} else {
			if(trie->right == NULL) trie->right = newNode();
			if(trie->right == NULL) return false;
			trie = trie->right;
		}

		nr /= 2;
	}

	if(trie->list != NULL) return false;
	trie->list = list;
	return true;
}

Trie* findTrie(int nr, Trie* trie)
{
	assert(trie != NULL);
	while(nr != 0) {
		if(nr % 2 == 0) {
			if(trie->left == NULL) return NULL;
			trie = trie->left;
		} else {
			if(trie->right == NULL) return NULL;
			trie = trie->right;
		}

		nr /= 2;
	}
	return trie;
}

void mergeTrie(Trie* a, Trie* b)
{
	if(b == NULL) return;
	mergeTrie(a->left, b->left);
	mergeTrie(a->right, b->right);

	if(b->list == NULL) return;

	List* list = nextList(a->list);

	while(list != NULL) {
		if(isSame(getValue(list), getFirst(b->list))) {
			insertList(list, b->list);
			return;
		}

		list = nextList(list);
	}
}

void delTrie(Trie* v)
{
	if(v == NULL) return;
	delTrie(v->left);
	delTrie(v->right);
	delList(v->list);
	free(v);
}

void setList(Trie* trie, List* list)
{
	trie->list = list;
}

List* getList(Trie* trie)
{
	if(trie == NULL) {
		return NULL;
	}
	return trie->list;
}