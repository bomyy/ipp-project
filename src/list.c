/** @file
implementacja klasy odpowiedzialej za klasę list.
*/
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "list.h"

/**
struktura odpowiedzialna za implementację klasy @p List
*/
typedef struct List {
	List* next; ///< wskaźnik na następny element listy
	City* city; ///< wskaźnik na element listy
} List;

List* newList()
{
	return calloc(1, sizeof(List));
}

bool addList(List* list, City* city)
{
	assert(list != NULL);
	assert(city != NULL);
	List* tmp = newList();
	if(tmp == NULL) return false;
	tmp->city = city;
	tmp->next = list->next;
	list->next = tmp;
	return true;
}

List* mergeList(List* list1, List* list2, int nr)
{
	assert(list1 != NULL);
	assert(list2 != NULL);

	List* ret = list1;
	while(list1->next != NULL) list1 = list1->next;
	List* tmp = list2;
	list2 = list2->next;
	free(tmp);
	if(nr == 1) {
		tmp = list2;
		list2 = list2->next;
		free(tmp);
	}
	while(list2 != NULL) {
		tmp = list2;
		if(nr == 2 && list2->next == NULL) {
			list2 = list2->next;
			free(list2);
			free(tmp);
			break;
		}
		list2 = list2->next;
		tmp->next = list1->next;
		list1->next = tmp;
		if(nr == 1) list1 = list1->next;
	}
	return ret;
}

List* findList(List* list, City* city)
{
	if(list == NULL) return NULL;
	list = list->next;
	while(list != NULL) {
		if(isSame(city, list->city)) return list;
		list = list->next;
	}
	return NULL;
}

void delList(List* list)
{
	while(list != NULL) {
		List* tmp = list->next;
		free(list);
		list = tmp;
	}
}

List* nextList(List* list)
{
	assert(list != NULL);
	return list->next;
}

City* getValue(List* list)
{
	if(list == NULL) return NULL;
	return list->city;
}

City* getFirst(List* list)
{
	if(list == NULL) return NULL;
	list = list->next;
	return list->city;
}

City* getLast(List* list)
{
	list = list->next;
	City* ret = NULL;

	while(list != NULL) {
		ret = list->city;
		list = list->next;
	}

	return ret;
}

void insertList(List* list, List* a)
{
	List* tmp = a->next;
	free(a);
	a = tmp;
	tmp = a->next;
	free(a);
	a = tmp;
	while(a->next != NULL) {
		tmp = a->next;
		a->next = list->next;
		list->next = a;
		a = tmp;
		list = list->next;
	}

	free(a);
}