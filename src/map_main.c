/** @file
plik odpowiedzialny za działanie całego projektu
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "read.h"
#include "map.h"

/**
@brief main
Wczytuje linie wejścia dopuki może, dla każdej linii sprawdza
czy jest poprawnym opisem funkcji, jeśli tak to ją wykonuje.
*/
int main()
{
    Map* map = newMap();

    for(int nr = 1; !isEndOfInput(); nr++) {
    	bool isErr = false;
    	const char* newLine = readLine();

    	if(newLine == NULL) {
    		isErr = true;
    		goto EndOfWhile;
    	}
		if(strlen(newLine) == 0) goto EndOfWhile;

    	if(newLine[0] == '#') goto EndOfWhile;

        if(isEndOfInput()) {
            isErr = true;
            goto EndOfWhile;
        }

    	if(newLine[0] == 'a') {
    		if(!addRoadLine(map, newLine)) isErr = true;
    		goto EndOfWhile;
    	}

    	if(newLine[0] == 'r') {
    		if(!repairRoadLine(map, newLine)) isErr = true;
    		goto EndOfWhile;
    	}

    	if(newLine[0] == 'g') {
    		if(!getRouteLine(map, newLine)) isErr = true;
    		goto EndOfWhile;
    	}

    	if(!addRouteLine(map, newLine)) isErr = true;

    	EndOfWhile:
    	free((char*)newLine);
    	if(isErr) fprintf(stderr, "ERROR %d\n", nr);
    }

    deleteMap(map);

    return 0;
}