/** @file
Implementacja klasy String
*/
/**
define srawia że przy kompilacji nie dostaje się warningów na temat strdup.
Bez niej pojawiają się warningi o braku deklaracji strdup, mimo dodania string.h
 */
#define _DEFAULT_SOURCE 1
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "name.h"

/**
* struktura odpowiedzialna za implementację klasy String
*/
typedef struct String {
	size_t size; ///< rozmiar stringa
	const char* name; ///< tablica charów którą string reprezentuje
} String;

String* newName(const char *name)
{
	String* newName = calloc(1, sizeof(String));
	if(newName == NULL) return NULL;
	newName->size = strlen(name);
	newName->name = strdup(name);
	if(newName->name == NULL) {
	  free(newName);
	  return NULL;
	}
	return newName;
}

const char* getString(String* x)
{
	return x->name;
}

size_t getSize(String* x)
{
	return x->size;
}

void delString(String* str)
{
	free((char*)str->name);
	free(str);
}

/**
@brief porównuje stringi
@param [in] a - wskaźnik na pierwszego stringa
@param [in] b - wskaźnik na drugiego stringa
@return poruwnuje czy @p a jest mniejszy leksykograficznie od @p b.
Jeśli jest mniejszy funkcja zwraca -1, jeśli jest mniejszy zwraca
1, jeśli @p a i @p b są takie same swraca 0.
*/
static int compareString(String* a, String* b)
{
	assert(a->size == b->size);
	const char* aChar = a->name;
	const char* bChar = b->name;
	for(unsigned long i = 0; i < a->size; i++) {
		if((*aChar) < (*bChar)) return -1;
		if((*aChar) > (*bChar)) return 1;
		aChar++;
		bChar++;
	}
	return 0;
}

bool lowerBound(String* a, String* b)
{
	if(a->size < b->size) return true;
	if(a->size > b->size) return false;
	if(compareString(a, b) < 0) return true;
	return false;
}

bool upperBound(String* a, String* b)
{
	if(a->size < b->size) return true;
	if(a->size > b->size) return false;
	if(compareString(a, b) <= 0) return true;
	return false;
}
