/** @file
Implementacja klasy przechowującej zbiór miast
*/
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "set.h"

/**
* struktura odpowiedzialna za implementację klasy @p Set
*/
typedef struct Set {
	int key; ///< losowy klucz
	long long year; ///< rok budowy drogi
	int length; ///< odległość do danego miasta
	City* value; ///< miasto
	Set* left; ///< wskaźnik na lewego syna
	Set* right; ///< wskaźnik na prawego syna
} Set;

/**
@p set pomocniczy, potrzebny aby funckje zawsze działały
i nie wykorzystywały malloca, nawet jeśli zabraknie pamięci
*/
static Set hel1;
/**
@p set pomocniczy, potrzebny aby funckje zawsze działały
i nie wykorzystywały malloca, nawet jeśli zabraknie pamięci
*/
static Set hel2;

/**
@brief podłącza @p left i @p right jako lewego i prawego syna Seta @p father

@param [in] father - @p Set do którego podłączamy synów
@param [in] left - wskaźnik na @p Set który ma być lewym synem @p father
@param [in] right - wskaźnik na @p Set który ma być prawym synem @p father
@return funkcja zwraca wskaźnik na father, który ma już podłączonych synów
*/
static Set* attach(Set* father, Set* left, Set* right)
{
	father->left = left;
	father->right = right;
	return father;
}

/**
@brief łączy dwa Sety w jeden

Przyjmuje dwa sety i tworzy z nich jeden.
Funkcja zakłada że wszystkie elementy @p left są mniejsze od
wszystkich elementów @p right
@param [in] left - wskaźnik na @p Set który chcemy złączyć
@param [in] right - wskaźnik na @p Set który chcemy złączyć
@return funkcja zwraca wskaźnik na @p Set zawierający wszystkie
elementy Seta @p left jak i @p right
*/
static Set* merge(Set* left, Set* right)
{
	if(left == NULL) return right;
	if(right == NULL) return left;
	if(left->key <= right->key) {
		return(attach(right, merge(left, right->left), right->right));
	} else {
		return(attach(left, left->left, merge(left->right, right)));
	}
}

/**
@brief dzieli Set na dwa

Dzieli wskazany Set na dwa, w jednym znajdują się elementy mniejsze
lub równe podanemu @p city, w drugim znajdują się elementy ściśle
większe.
@param [in] node - wskaźnik na @p Set który dzielimy
@param [in] city - wskaźnik na @p City po którym dzielimy dany @p Set
@param [in] acc - akumulator
@return funkcja przypina Set z elementami mniejszymi jako lewego syna
akumulatora, a Set z elementami większymi do prawego syna akumulatora
*/
static void splitUpperBound(Set* node, City* city, Set* acc)
{
	if(node == NULL) return;
	if(compUpperBound(node->value, city)) {
		splitUpperBound(node->right, city, acc);
		attach(acc, attach(node, node->left, acc->left), acc->right);
	} else {
		splitUpperBound(node->left, city, acc);
		attach(acc, acc->left, attach(node, acc->right, node->right));
	}
}

/**
@brief dzieli Set na dwa

Dzieli wskazany Set na dwa, w jednym znajdują się elementy mniejsze
podanemu @p city, w drugim znajdują się elementy ściśle większe lub równe.
@param [in] node - wskaźnik na @p Set który dzielimy
@param [in] city - wskaźnik na @p City po którym dzielimy dany @p Set
@param [in] acc - akumulator
@return funkcja przypina Set z elementami mniejszymi jako lewego syna
akumulatora, a Set z elementami większymi do prawego syna akumulatora
*/
static void splitLowerBound(Set* node, City* city, Set* acc)
{
	if(node == NULL) return;
	if(compLowerBound(node->value, city)) {
		splitLowerBound(node->right, city, acc);
		attach(acc, attach(node, node->left, acc->left), acc->right);
	} else {
		splitLowerBound(node->left, city, acc);
		attach(acc, acc->left, attach(node, acc->right, node->right));
	}
}

/**
@brief tworzy nowy wierzchołek

Przyjmuje wskaźnik na miasto oraz długość i rok budowy drogi.
Tworzy nowy Set zawierający jeden element.
@param [in] city - wskaźnik na @p City które chcemy zamienić na Set
@param [in] length - długość drogi
@param [in] year - rok budowy drogi
@return funkcja zwraca wskaźnik na @p Set zawierający miasto @p city
*/
static Set* newNode(City* city, int year, long long length)
{
	Set* ret = calloc(1, sizeof(Set));
	if(ret == NULL) return NULL;
	ret->value = city;
	ret->key = rand();
	ret->length = length;
	ret->year = year;
	return ret;
}

Set* newSet()
{
	Set* re = NULL;
	return re;
}

void delAllSet(Set* v)
{
	if(v == NULL) return;
	delAllSet(v->left);
	delAllSet(v->right);
	free(v);
}

Set* findSet(Set* set, City* city)
{
	if(set == NULL) return NULL;
	if(isSame(set->value, city)) return set;
	if(compUpperBound(set->value, city)) return findSet(set->right, city);
	return findSet(set->left, city);
}

Set* addSet(Set* node, City* toAdd, int year, long long length)
{
	if(findSet(node, toAdd) != NULL) return node;
	Set* set = newNode(toAdd, year, length);
	if(set == NULL) return NULL;
	Set* tmp = attach(&hel1, NULL, NULL);
	splitLowerBound(node, toAdd, tmp);
	Set* ret = merge(tmp->left, set);
	ret = merge(ret, tmp->right);
	return ret;
}

Set* delSet(Set* node, City* city)
{
	if(findSet(node, city) == NULL) return node;
	Set* l = attach(&hel1, NULL, NULL);
	Set* r = attach(&hel2, NULL, NULL);
	splitLowerBound(node, city, l);
	splitUpperBound(l->right, city, r);
	free(r->left);
	Set* ret = merge(l->left, r->right);
	return ret;
}

bool changeYear(Set* set, int newYear)
{
	if(set->year > newYear) return false;
	set->year = newYear;
	return true;
}

City* getCity(Set* v)
{
	if(v == NULL) return NULL;
	return v->value;
}

Set* getLeft(Set* v)
{
	assert(v != NULL);
	return v->left;
}

Set* getRight(Set* v)
{
	assert(v != NULL);
	return v->right;
}

long long getLength(Set* set)
{
	return set->length;
}

int getYearSet(Set* set)
{
	return set->year;
}

void clearPredecessor(Set* set)
{
	if(set == NULL) return;
	clearPredecessor(getLeft(set));
	clearPredecessor(getRight(set));
	prepareSet(getCity(set));
}
