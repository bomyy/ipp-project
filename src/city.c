#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include "city.h"

const int VISITED = 2;
const int NOT_VISITED = 1;
const int NOT_DISTINCT = 0;

City* newCity(const char* name)
{
	City* tmp = calloc(1, sizeof(City));
	if(tmp == NULL) return NULL;
	tmp->name = newName(name);
	if(tmp->name == NULL) {
		free(tmp);
		return NULL;
	}
	tmp->predecessor = NULL;
	tmp->status = NOT_VISITED;
	return tmp;
}

String* getName(City* city)
{
	return city->name;
}

void delCity(City* city)
{
	delString(city->name);
	delAllSet(city->neighbour);
	free(city);
}

bool compLowerBound(City* a, City* b)
{
	return lowerBound(a->name, b->name);
}

bool compUpperBound(City* a, City* b)
{
	return upperBound(a->name, b->name);
}

bool isSame(City* a, City* b)
{
	if(a == NULL || b == NULL) return false;
	if(compUpperBound(a, b) && compUpperBound(b, a)) {
		return true;
	}
	return false;
}

Set* getNeighbour(City* city)
{
	return city->neighbour;
}

void setNeighbour(City* city, Set* set)
{
	city->neighbour = set;
}

void setDis(City* city, long long dis, int year)
{
	city->year = year;
	city->weight = dis;
}

int getSecondYear(City* city)
{
	return city->secondYear;
}

void setSecondYear(City* city, int year)
{
	city->secondYear = year;
}

int getYear(City* city)
{
	return city->year;
}

long long getDis(City* city)
{
	return city->weight;
}

void prepareSet(City* city)
{
	city->weight = LLONG_MAX;
	city->year = INT_MAX;
	city->secondYear = INT_MIN;
	city->predecessor = NULL;
}

int getStatus(City* city)
{
	return city->status;
}

void setStatus(City* city, int status)
{
	assert(status >= 0 && status <= 2);
	city->status = status;
}

City* getPredecessor(City* city)
{
	return city->predecessor;
}

void setPredecessor(City* city, City* pre)
{
	city->predecessor = pre;
}