/** @file
 Interfejs klasy przechowującej zbiór miast

 @author Konrad Czapliński <kc406122@mimuw.edu.pl>
 */
#ifndef __SET_H__
#define __SET_H__

/**
@brief struktura odpowiedzialna za implementację seta.
*/
typedef struct Set Set;

#include "city.h"

/**
tworzy nowy @p Set.
@return zwraca nowy @p Set, lub @p NULL jeśli nie udało się
zalokować pamięci.
*/
Set* newSet();

/**
@brief dodaje do @p Node miasto z rokiem budowy @p year oraz odległością @p length

@param[in] node - wskaźnik na set do którego dodajemy miasto
@param[in] toAdd - miasto które dodajemy
@param[in] year - rok wybudowania drogi do miasta @p toAdd
@param[in] length - długość drobi prowadzącej do miasta @p toAdd
@return Zwraca wartość wskaźnik na @p Set z dodanym miastem
*/
Set* addSet(Set* node, City* toAdd, int year, long long length);

/**
@brief usuwa z set'a miasto @p City

@param[in] node - wskaźnik na Seta z którego usuwamy miasto
@param[in] city - wskaźnik na miasto które chcemy usunąć
, nie musi on wskazywać na element który usuniemy, wystarczy
aby jego nazwa była taka sama jak nazwa miasta które chcemy usunąć
@return Zwraca wskaźnik na @p Set z usuniętym miastem @p City
*/
Set* delSet(Set* node, City* city);

/**
@brief zwraca wskaźnik na miasto nalerzące do danego wierzchołka

@param[in] set - wskaźnik na set z którego chcemy uzyskać miasto.
@return zwraca miasto trzymane przez podany wierzchołek seta.
*/
City* getCity(Set* set);

/**
@brief wyszukuje w secie miasto z podaną nazwą

@param[in] set - wskaźnik na korzeń seta w którym chcemy szukać miasta
@param[in] city - miasto z które chcemy w danym secie znaleść
@return Zwraca wskaźnik na miasto z taką samą nazwą jak @p City, lub @p NULL
jeśli nie ma takiego miasta.
*/
Set* findSet(Set* set, City* city);

/**
@brief usuwa cały @p Set

@param[in] set - wskaźnik na korzeń seta którego chcemy usunąć
wywołuje się rekurencyjnie, usuwa wszystkie wierzchołki nalerzące od danego
seta, ale nie miasta.
*/
void delAllSet(Set* set);

/**
@brief zmienia rok budowy drogi

@param[in] set - wskaźnik na wierzchołek w którym chcemy zmienić rok budowy
@param[in] year - nowy rok budowy
@return zwraca @p true jeśli zmiana się powiodła lub @p false jeśłi wystąpił błąd:
podany rok jest starszy niż poprzedni
*/
bool changeYear(Set* set, int year);

/**
@brief zwraca lewego syna wierzchołka

@param[in] set - numer wierzchołka o który pytamy
@return zwraca lewego syna podanego wierzchołka lub NULL jeśli takowy nie istnieje
*/
Set* getLeft(Set* set);

/**
@brief zwraca prawego syna wierzchołka

@param[in] set - numer wierzchołka o który pytamy
@return zwraca prawego syna podanego wierzchołka lub NULL jeśli takowy nie istnieje
*/
Set* getRight(Set* set);

/**
@brief przygodowuje całe poddrzewo naobliczenie odległości
ustawia wartości year oraz odległość w każdym mieście znajdującym się
w danym secie na nigdy nieosiągalne
@param[in] set - wskaźnik na korzeń seta którego chcemy przygotować
*/
void clearPredecessor(Set* set);

/**
@brief zwraca odległość do danego miasta przechowywanego w danym wierzchołku

@param[in] set - wierzchołek z miastek o które chcemy zapytać

@return zwraca odległość do danego miasta.
*/
long long getLength(Set* set);

/**
@brief zwraca rok budowy/remontu drogi prowadzącej
do danego miasta przechowywanego w danym wierzchołku

@param[in] set - wierzchołek z miastek o które chcemy zapytać

@return zwraca rok budowy lub ostatniego remontu drogi
do podanego miasta.
*/
int getYearSet(Set* set);
#endif