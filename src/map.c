/* @file
Implementacja klasy map.
*/
/**
define srawia że przy kompilacji nie dostaje się warningów na temat strdup.
Bez niej pojawiają się warningi o braku deklaracji strdup, mimo dodania string.h
 */
#define _DEFAULT_SOURCE 1
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "map.h"
#include "set.h"
#include "trie.h"
#include "city.h"
#include "name.h"
#include "read.h"

const int nrOfRoutes = 1000;

/**
 * Struktura przechowująca mapę dróg krajowych.
 */
typedef struct Map {
	Set* cities; ///< @p Set przechowujący wszystkie miasta nalerzące do danej mapy
	Trie* routes; ///< @p Trie przechowujące wszystkie drogi krajowe danej mapy
} Map;

static void delDfs(Set* v)
{
	if(v == NULL) return;
	delDfs(getLeft(v));
	delDfs(getRight(v));
	delCity(getCity(v));
}

static Set* findCity(Map* map, const char* name)
{
	Set* tmp = NULL;
	City* c1 = newCity(name);
	if(c1 == NULL) goto clearFindCity;

	tmp = findSet(map->cities, c1);

	clearFindCity:
	delCity(c1);
	return tmp;
}

static List* getRouteTo(City* city, City* finish)
{
	List* list = newList();
	if(list == NULL) return NULL;

	while(!isSame(city, finish)) {
		if(city == NULL) {
			delList(list);
			return NULL;
		}
		if(!addList(list, city)) {
			delList(list);
			return false;
		}

		city = getPredecessor(city);
	}

	if(!addList(list, city)) {
		delList(list);
		return false;

	}
	return list;
}

static bool addChar(char** ret, size_t* size, size_t* id, char c)
{
	if((*id) == (*size)) {
		(*size) = 2 * (*size);
		char* tmp = realloc(*ret, (*size) * sizeof(char));
		if(tmp == NULL) {
			free((*ret));
			return false;
		}
		(*ret) = tmp;
		ret = (&tmp);
	}

	size_t zm = (*id);
	(*ret)[zm] = c;
	(*id) ++;
	return true;
}

static bool addNumber(char** ret, size_t* size, size_t* id, int add)
{
	char liczba[15];
	sprintf(liczba, "%d", add);
	for(int i = 0; liczba[i] != '\0'; i++) {
		if(!addChar(ret, size, id, liczba[i])) return false;
	}
	return true;
}

static bool isNameOk(const char* name)
{
	const char* it = name;
	while((*it) != '\0') {
		if((*it) >= 0 && (*it) <= 31) return false;
		if((*it) == ';') return false;
		it ++;
	}

	return true;
}

static bool isYearOK(int year)
{
	if(year != 0) return true;
	return false;
}

static bool isRouteIdOk(int routeId)
{
	if(routeId > 0 && routeId < nrOfRoutes) return true;
	return false;
}

static bool isLenghtOk(int length)
{
	if(length > 0) return true;
	return false;
}

static bool isMapOk(Map* map)
{
	if(map == NULL) return false;
	return true;
}

Map* newMap(void)
{
	Map* m = calloc(1, sizeof(Map));
	if(m == NULL) return NULL;
	m->routes = newNode();
	if(m->routes == NULL) {
		free(m);
		return NULL;
	}
	return m;
}

void deleteMap(Map *map)
{
	delDfs(map->cities);
	delTrie(map->routes);
	delAllSet(map->cities);
	free(map);
}

bool addRoad(Map *map, const char *city1, const char *city2, unsigned length, int builtYear)
{
	if(!isMapOk(map)) return false;
	if(!isNameOk(city1)) return false;
	if(!isNameOk(city2)) return false;
	if(!isLenghtOk(length)) return false;
	if(!isYearOK(builtYear)) return false;
	bool isS1New = false;
	bool isS2New = false;
	Set* s1 = NULL;
	Set* s2 = NULL;
	Set* tmp = NULL;


	City* c1 = newCity(city1);
	City* c2 = newCity(city2);
	if(c2 == NULL || c1 == NULL) goto FailedToAddRoad;
	if(isSame(c1, c2)) goto FailedToAddRoad;

	s1 = findSet(map->cities, c1);
	s2 = findSet(map->cities, c2);

	if(s1 == NULL) {
		s1 = addSet(map->cities, c1, builtYear, length);
		isS1New = true;
		if(s1 == NULL) goto FailedToAddRoad;
		else map->cities = s1;
		s1 = findSet(map->cities, c1);
	}

	if(s2 == NULL) {
		s2 = addSet(map->cities, c2, builtYear, length);
		isS2New = true;
		if(s2 == NULL) goto FailedToAddRoad;
		else map->cities = s2;
		s2 = findSet(map->cities, c2);
	}

	if(findSet(getNeighbour(getCity(s1)), getCity(s2)) != NULL) goto RoadExists;
	if(findSet(getNeighbour(getCity(s2)), getCity(s1)) != NULL) goto RoadExists;

	tmp = addSet(getNeighbour(getCity(s1)), getCity(s2), builtYear, length);
	if(tmp == NULL) goto FailedToAddRoad;
	else setNeighbour(getCity(s1), tmp);
	tmp = addSet(getNeighbour(getCity(s2)), getCity(s1), builtYear, length);
	if(tmp == NULL) goto FailedToAddRoad;
	else setNeighbour(getCity(s2), tmp);

	if(!isS1New) delCity(c1);
	if(!isS2New) delCity(c2);

	return true;

	FailedToAddRoad:
	if(s1 != NULL) delSet(s1, c2);
	if(s2 != NULL) delSet(s2, c1);
	if(isS1New) delSet(map->cities, getCity(s1));
	if(isS2New) delSet(map->cities, getCity(s2));

	RoadExists:
	delCity(c1);
	delCity(c2);
	return false;
}

bool repairRoad(Map *map, const char *city1, const char *city2, int repairYear)
{
	if(!isMapOk(map)) return false;
	if(!isNameOk(city1)) return false;
	if(!isNameOk(city2)) return false;
	if(!isYearOK(repairYear)) return false;

	Set* tmp1 = findCity(map, city1);
	Set* tmp2 = findCity(map, city2);

	if(tmp1 == NULL || tmp2 == NULL) return false;

	Set* nei1 = findSet(getNeighbour(getCity(tmp1)), getCity(tmp2));
	Set* nei2 = findSet(getNeighbour(getCity(tmp2)), getCity(tmp1));

	if(nei1 == NULL || nei2 == NULL) return false;
	if(!changeYear(nei1, repairYear)) return false;
	if(!changeYear(nei2, repairYear)) return false;
	return true;
}

char const* getRouteDescription(Map *map, unsigned routeId)
{
	if(!isMapOk(map)) return strdup("");
	if(!isRouteIdOk(routeId)) return strdup("");

	List* list = getList(findTrie(routeId, map->routes));
	if(list == NULL) return strdup("");
	list = nextList(list);
	if(list == NULL) return strdup("");

	size_t size = 1;
	size_t id = 0;
	char* ret = calloc(1, sizeof(char));
	if(ret == NULL) {
		free(ret);
		return strdup("");
	}
	if(!addNumber(&ret, &size, &id, routeId)) return strdup("");
	if(!addChar(&ret, &size, &id, ';')) return strdup("");

	while(list != NULL) {
		size_t tmp = getSize(getName(getValue(list)));
		for(size_t i = 0; i < tmp; i++) {
			char c = getString(getName(getValue(list)))[i];
			if(!addChar(&ret, &size, &id, c)) return strdup("");
		}
		if(nextList(list) == NULL) break;

		Set* set = getNeighbour(getCity(findSet(map->cities, getValue(list))));
		set = findSet(set, getValue(nextList(list)));

		if(!addChar(&ret, &size, &id, ';')) return strdup("");
		if(!addNumber(&ret, &size, &id, getLength(set))) return strdup("");
		if(!addChar(&ret, &size, &id, ';')) return strdup("");
		if(!addNumber(&ret, &size, &id, getYearSet(set))) return strdup("");
		if(!addChar(&ret, &size, &id, ';')) return strdup("");

		list = nextList(list);
	}

	char* ans = calloc(id + 1, sizeof(char));
	if(ans == NULL) {
		free(ret);
		return strdup("");
	}

	for(size_t i = 0; i < id; i ++) {
		ans[i] = ret[i];
	}
	free(ret);
	return ans;
}

bool addRoute(Map* map, const char* str)
{
	int routeId = getInt(str, 0);
	if(!addTrie(routeId, NULL, map->routes)) return false;
	clearPredecessor(map->cities);
	int name1 = nextStop(str, 0);

	//sprawdzenie czy dana ścieżka jest możliwa
	while(str[nextStop(str, name1)] != '\0') {
		int length = nextStop(str, name1);
		int year = nextStop(str, length);
		int name2 = nextStop(str, year);
		char* str1 = (char*)strndup(str + name1, nextStop(str, name1) - name1 - 1);
		char* str2;
		if(str[nextStop(str, name2)] == '\0') str2 = (char*)strndup(&str[name2], nextStop(str, name2) - name2);
		else str2 = (char*)strndup(&str[name2], nextStop(str, name2) - name2 - 1);

		if(strcmp(str1, str2) == 0) {
			free(str1);
			free(str2);
			return false;
		}

		Set* s1 = findCity(map, str1);
		Set* s2 = findCity(map, str2);
		if(s1 == NULL || s2 == NULL) {
			if(s1 == NULL) {
				City* cityTmp = newCity(str1);
				if(cityTmp == NULL) return false;
				Set* setTmp = addSet(map->cities, cityTmp, 0, 0);

				if(setTmp == NULL) {
					delCity(cityTmp);
					return false;
				}

				map->cities = setTmp;
			}

			if(s2 == NULL) {
				City* cityTmp = newCity(str2);
				if(cityTmp == NULL) return false;
				Set* setTmp = addSet(map->cities, cityTmp, 0, 0);

				if(setTmp == NULL) {
					delCity(cityTmp);
					return false;
				}

				map->cities = setTmp;
			}
			goto EndOfWhile1;
		}

		s1 = getNeighbour(getCity(s1));
		if(s1 == NULL) goto EndOfWhile1;
		s1 = findSet(s1, getCity(s2));
		if(s1 == NULL) goto EndOfWhile1;

		if(getLength(s1) != getInt(str, length)) {
			free(str1);
			free(str2);
			return false;
		}

		if(getYearSet(s1) > getInt(str, year)) {
			free(str1);
			free(str2);
			return false;
		}

		EndOfWhile1:
		s1 = findCity(map, str1);
		s2 = findCity(map, str2);

		if(getPredecessor(getCity(s2)) != NULL) {
			free(str1);
			free(str2);
			return false;
		}
		setPredecessor(getCity(s2), getCity(s1));

		name1 = name2;
		free(str1);
		free(str2);
	}

	name1 = nextStop(str, 0);
	char* strFirst = (char*)strndup(str + name1, nextStop(str, name1) - name1 - 1);
	City* c1 = getCity(findCity(map, strFirst));
	if(getPredecessor(c1) != NULL) {
		free(strFirst);
		return false;
	}
	setPredecessor(c1, c1);
	free(strFirst);

	name1 = nextStop(str, 0);
	City* c2 = NULL;

	while(str[nextStop(str, name1)] != '\0') {
		int length = nextStop(str, name1);
		int year = nextStop(str, length);
		int name2 = nextStop(str, year);
		char* str1 = (char*)strndup(str + name1, nextStop(str, name1) - name1 - 1);
		char* str2;
		if(str[nextStop(str, name2)] == '\0') str2 = (char*)strndup(&str[name2], nextStop(str, name2) - name2);
		else str2 = (char*)strndup(str + name2, nextStop(str, name2) - name2 - 1);
		Set* s1 = findCity(map, str1);
		Set* s2 = findCity(map, str2);
		if(s1 == NULL || s2 == NULL) goto EndOfWhile2;

		s1 = getNeighbour(getCity(s1));
		if(s1 == NULL) goto EndOfWhile2;
		s1 = findSet(s1, getCity(s2));
		if(s1 == NULL) goto EndOfWhile2;

		if(!repairRoad(map, str1, str2, getInt(str, year))) return false;
		c2 = getCity(findCity(map, str2));
		free(str1);
		free(str2);
		name1 = name2;
		continue;

		EndOfWhile2:
		if(!addRoad(map, str1, str2, getInt(str, length), getInt(str, year))) {
			free(str1);
			free(str2);
			return false;
		}
		c2 = getCity(findCity(map, str2));
		free(str1);
		free(str2);
		name1 = name2;
	}

	name1 = nextStop(str, 0);
	strFirst = (char*)strndup(str + name1, nextStop(str, name1) - name1 - 1);
	c1 = getCity(findCity(map, strFirst));
	free(strFirst);

	List* list = getRouteTo(c2, c1);
	if(list == NULL) {
		return false;
	}
	if(!addTrie(routeId, list, map->routes)) {
		delList(list);
		return false;
	}

	return true;
}
