/** @file
 * Interfejs klasy przechowującej listę miast
 *
 * @author Konrad Czapliński <kc406122@mimuw.edu.pl>
 */
#ifndef __LIST_H__
#define __LIST_H__

/**
struktura odpowiedzialna za implementację klasy List
*/
typedef struct List List;

#include "city.h"

/**
@brief tworzy nową listę

@return funkcja zwraca wskaźnik na nowo utworzoną listę,
lub NULL jeśli nie udało się zaalokować pamięci
*/
List* newList();

/**
@brief znajduje podane miasto na liście

Znajduje i zwraca wskaźnik na pierwszy element równy @p city,
lub NULL jeśli taki element nie istnieje.

@param [in] list - wskaźnik na początek listy którą chcemy przeszukać
@param [in] city - miasto które szukamy
@return funkcja zwraca wskaźnik na pierwszy element równy @p city,
lub NULL jeśli taki element nie istnieje.
*/
List* findList(List* list, City* city);

/**
@brief dodaje element do listy.

dodaje podany element do listy, pomiędzy @p list i jego następnika

@param [in] list - wskaźnik na element listy za którym dodajemy @p city
@param [in] city - miasto które dodajemy
@return funkcja zwraca true jeśli dodanie miasta się powiodło,
i false jeśli zabrakło pamięci
*/
bool addList(List* list, City* city);

/**
@brief łączy dwie listy w jedną

oznaczmy elementy pierwszej listy jako:
[l1a, l1b, ...,]
natomiast drógiej listy jako:
[l2a, l2b, ...,]
łączy dwie listy w jedną. jeśli @p nr wynosi 1 to wynikowa lista wygląda
następująco:
[l1a, l1b, ..., l1n, l2a, l2b, ... ]
natomiast jeśli nr wynosi dwa to wynikowa lista wynosi:
[l1a, l1b, ..., l1n, l2n, l2l, ... ]

@param [in] list1 - wskaźnik na początek pierwszej listy
@param [in] list2 - wskaźnik na początek drógiej listy
@param [in] nr - liczba wskazująca w jaki sposób przebiega łączenie
@return funkcja zwraca wskaźnik na pierwszy element wynikowej listy
*/
List* mergeList(List* list1, List* list2, int nr);

/**
@brief usuwa podaną listę.

usuwa wszystkie elementy listy, zaczynając od podanego.

@param [in] list - wskaźnik na listę którą usuwamy
*/
void delList(List* list);

/**
@brief zwraca następny element listy

zwraca następnika podanego elementu listy. Jeśli nie ma on już
następnika, to zwraca NULL.

@param [in] list - wskaźnik na element listy o który pytamy
@return funkcja zwraca wskaźnik na następny element listy,
lub NULL jeśli taki nie istnieje.
*/
List* nextList(List* list);

/**
@brief zwraca wskaźnik na element listy

@param [in] list - wskaźnik na element listy o który pytamy
@return funkcja zwraca wskaźnik na miasto będące przechowywane
w danym elemencie listy. Jeśli list wynosi NULL to funkcja zwraca NULL.
*/
City* getValue(List* list);

/**
@brief zwraca pierwszy element listy.

@param [in] list - wskaźnik na listę o którą pytamy
@return funkcja zwraca wskaźnik na miasto będące na początku
listy, lub NULL jeśli podana lista jest pusta.
*/
City* getFirst(List* list);

/**
@brief zwraca ostatni element listy.

@param [in] list - wskaźnik na listę o którą pytamy
@return funkcja zwraca wskaźnik na miasto będące na końcu
listy, lub NULL jeśli podana lista jest pusta.
*/
City* getLast(List* list);

/**
@brief dodaje listę a na początek listy list

@param [in] list - wskaźnik na listę do której dołączamy a
@param [in] a - wskaźnik na listę którą dołączamy
*/
void insertList(List* list, List* a);
#endif